# My awesome react tutorial

### Authors
ZENATI Lamine, BEUNS Vianney, ATMANE Bilal 

### Introduction
Welcome to an awesome tutorial to learn **React** !
If you want to be an awesome react developper, you're at the right place.

With this tutorial we won't simply give you pre-made solutions to build the App of your Dream but rather give you some <code>evil issues</code> and some  <code>weapons</code> to face them, everything else is up to you  ! 

### Disclaimer
This tutorial has been designed for educational purpose only, we therefore cannot be held responsible for any headache, bricked brain, or broken relationship after following the incoming steps.
We are professional developpers who followed an intensive training to be able to offer you this performance.
**Please do not try this at home !**

# Let's Go !
## First step
Now that you are warned, we can go ahead and get our first step into building an **awesome react App** !
First things first we'll need to build our project directory to have a place to get started.
You should now all be familiar with <code>NPM</code>. Well, building a sample react App is as simple as running the following command in the folder of your choice : 
```bash
npx create-react-app YOUR_APP_NAME
```
*( I hope you don't like code snippets like those because there won't be much ! )*

If everything went well, a directory with the name you chose should've been created. You can check that everything works fine by cd-ing into it and running <code>npm start</code>.
You should now see a beautifull React logo running in your browser at http://localhost:3000.

#### Optionnal step:
If you'd like to use the **CSS** library we used in the demo, you'll also need to run this in your project directory : 
```bash
npm install reactbulma
```

## The Real deal

### Use the props, luke
Now that you're ready to get started, let's truly get into react by creating your first Component.
What i'll ask you to warm up is to reproduce what i've done in the Demo:

**Create a search bar** whose value will be displayed on the screen as the input is updated. This shouldn't be too hard as I already showed you how to do it entirely, but just in case here are some quick reminders : 
- **Props** is short for properties, these are values passed from parent to children
- **State** holds values which represent the actual state of the component *( thanks captain obvious )*. When the state is updated, the component is updated as well as it children.

*hint: Our search bar component's only role is to hold input, it is his parent's responsibility to treat it !*

### Speeding up a bit

Ok, this was a pretty easy and straightforward task for you to build this component. Let's move on !
The goal of this tutorial is to build a page displaying a set of articles. At the end you should get something like this :

![Home page](./shopping.png)
<br/>*( Picture not contractual )*

As you can see, apart from the search bar in this page, there is a list of randomly generated articles.
This is where you'll get the use of react **component's** reusability.
A bit like in **OOP**, we'll build some **components** that we'll use to build some other **components** ... ( i think you get the idea here ).

But don't worry, we'll achieve this step by step. First, I want you to **create a single Article**
component that should display a **picture**, a **name** and a **price**.
Of course these 3 pieces of data shouldn't be static but parameterizable when using the component.

### Component-ception

When you are done building the **Article**, the next logical step is to display more than just one of them. Again i don't expect you to dumbly display hundreds of articles in your App component, you should rather create a new component, let's say **ArticleList** that will hold them.
Yet again, the data of the **ArticleList** should of course not be defined inside the component.
<br/><br/>
***Tip*:**<br/>
To test your component we put a Json file containing random generated data at your disposal.
Feel free to include it in your App by using the following import : 
```javascript
import data from './MOCK_DATA-1.json';
```

## Checkpoint
At this point you should now have a **SearchBar** component, an **Article** component and an **ArticleList** component.
In your App you should have <code>\<SearchBar\></code> and <code>\<ArticleList\></code> used but <code>\<Article\></code> should never be used directly outside of <code>\<ArticleList\></code>.
If this is not the case, you probably missed something. Try to double check the instructions and don't hesitate to call for help, we only bite if we are hungry :D

## Linking part

If everything is ok, let's move on to the next part. You'r gonna link everything you've made so far up. 
But first, let's do something else. If you imported the sample data you noticed that there is a lot of articles and that displaying them all on the page is not the best thing.
What i'll ask from you is to **display only 12 articles** and **create two buttons** <code>next</code> and <code>previous</code> to navigate through the items.

This shouldn't take you long. Right after you got that set up we'll finally link our search bar and our articles. Remember when we created the search bar, we displayed the text in it when it was modified. I now want you to **remove this display** and instead **filter the articles** to keep only those whose name contains the typed text.

*Hint:
 Be carefull with the order in which you filter for name, and filter the number of items. You need to filter all items not only displayed one*

## Go a little further
Now that you have a dynamic list of articles, the next step is to create a basket. The user can click on an article in the catalog to add it to his basket, the items in the basket will have to be displayed on the page, with the total amount to be paid.
Finally, the user can remove an article from his basket by clicking on it.
 
## Voila !  
 
 You are now done. You can be proud of you ! You are now an awesome react-dev !
<br/>**Congrats !!**